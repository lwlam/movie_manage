<%--
  Created by IntelliJ IDEA.
  User: 10255
  Date: 2020/12/17
  Time: 9:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>欢迎页面-L-admin1.0</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/theme2.css">
</head>
<body>
<div class="x-body layui-anim layui-anim-up">
    <blockquote class="layui-elem-quote x-red layui-bg-cyan">欢迎管理员&nbsp;&nbsp;&nbsp;
        <span id="dateTime" style="color: black"> </span>
    </blockquote>
    <fieldset class="layui-elem-field">
        <legend>数据统计</legend>
        <div class="layui-field-box">
            <div class="layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-body">
                        <div class="layui-carousel x-admin-carousel x-admin-backlog" lay-anim="" lay-indicator="inside" lay-arrow="none" style="width: 100%; height: 90px;">
                            <div carousel-item="">
                                <ul class="layui-row layui-col-space10 layui-this">
                                    <li class="layui-col-xs2 layui-bg-red">
                                        <a href="javascript:;" class="x-admin-backlog-body layui-bg-red">
                                            <h3>新闻数</h3>
                                            <p>
                                                <cite class="layui-bg-red">${news_number}</cite></p>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs2 layui-bg-red">
                                        <a href="javascript:;" class="x-admin-backlog-body layui-bg-red">
                                            <h3>电影数</h3>
                                            <p class="layui-bg-red">
                                                <cite class="layui-bg-red">${movie_number}</cite></p>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs2 layui-bg-orange">
                                        <a href="javascript:;" class="x-admin-backlog-body layui-bg-orange">
                                            <h3>电影类别数</h3>
                                            <p class="layui-bg-orange">
                                                <cite class="layui-bg-orange">${movieType_number}</cite></p>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs2 layui-bg-orange">
                                        <a href="javascript:;" class="x-admin-backlog-body layui-bg-orange">
                                            <h3>新闻类别数</h3>
                                            <p class="layui-bg-orange">
                                                <cite class="layui-bg-orange">${newsType_number}</cite></p>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs2 layui-bg-blue">
                                        <a href="javascript:;" class="x-admin-backlog-body layui-bg-blue">
                                            <h3>今日新用户</h3>
                                            <p class="layui-bg-blue">
                                                <cite class="layui-bg-blue">4</cite></p>
                                        </a>
                                    </li>
                                    <li class="layui-col-xs2 layui-bg-blue" >
                                        <a href="javascript:;" class="x-admin-backlog-body layui-bg-blue" >
                                            <h3>当前在线人数</h3>
                                            <p class="layui-bg-blue">
                                                <cite class="layui-bg-blue">60</cite></p>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    <div class="x-body">
        <blockquote class="layui-elem-quote layui-bg-green">
           今日流量数据展示
        </blockquote>
        <!-- 为 ECharts 准备一个具备大小（宽高）的 DOM -->
        <div id="main" style="width: 100%;height:400px;"></div>
    </div>
    <script src="//cdn.bootcss.com/echarts/3.3.2/echarts.min.js" charset="utf-8"></script>
    <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));

        // 指定图表的配置项和数据
        var option = {
            title: {
                text: '折线图堆叠'
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data:['邮件营销','联盟广告','视频广告','直接访问','搜索引擎']
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: ['周一','周二','周三','周四','周五','周六','周日']
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name:'邮件营销',
                    type:'line',
                    stack: '总量',
                    data:[120, 132, 101, 134, 90, 230, 210]
                },
                {
                    name:'联盟广告',
                    type:'line',
                    stack: '总量',
                    data:[220, 182, 191, 234, 290, 330, 310]
                },
                {
                    name:'视频广告',
                    type:'line',
                    stack: '总量',
                    data:[150, 232, 201, 154, 190, 330, 410]
                },
                {
                    name:'直接访问',
                    type:'line',
                    stack: '总量',
                    data:[320, 332, 301, 334, 390, 330, 320]
                },
                {
                    name:'搜索引擎',
                    type:'line',
                    stack: '总量',
                    data:[820, 932, 901, 934, 1290, 1330, 1320]
                }
            ]
        };


        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    </script>
        <fieldset class="layui-elem-field">
        <legend>开发团队</legend>
        <div class="layui-field-box">
            <table class="layui-table">
                <tbody>
                <tr>
                    <th>开源地址</th>
                    <td><p class="text-white">Copyright © 2020 <a href="https://gitee.com/lwlam/movie_manage" target="_top" style="color: #0e9aef">速影影院</a>. All right reserved</p></td>
                </tr>
                <tr>
                    <th>邮箱</th>
                    <td>2573864396@qq.com 2778442861@qq.com</td>
                </tr>
                </tbody>
            </table>
        </div>
    </fieldset>

</div>
<script>
    Date.prototype.format = function (fmt) {
        var o = {
            "y+": this.getFullYear, //年
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "h+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds() //秒
        };
        if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }
    setInterval("document.getElementById('dateTime').innerHTML = (new Date()).format('yyyy-MM-dd hh:mm:ss');", 1000);
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?b393d153aeb26b46e9431fabaf0f6190";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

</body>
</html>