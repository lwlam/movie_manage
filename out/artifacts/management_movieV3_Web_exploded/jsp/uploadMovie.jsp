<%--
  Created by IntelliJ IDEA.
  User: 10255
  Date: 2020/12/17
  Time: 9:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <meta charset="UTF-8">
    <title>电影后台管理系统</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<div class="x-body">
    <form class="layui-form" method="post" action="#" id="movie_upload_form">
        <div class="layui-form-item">
            <label for="username" class="layui-form-label">
                <span class="x-red">*</span>电影名
            </label>
            <div class="layui-input-inline">
                <input type="text" id="username" name="name" required lay-verify="required"
                       autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label for="username" class="layui-form-label">
                <span class="x-red">*</span>电影类别
            </label>
            <div class="layui-input-inline">
                <select name="type" id="movieType">
                    <option value="">类别</option>
                    <c:forEach items="${sessionScope.movieTypeList}" var="movieType">
                        <option value="${movieType.type}">${movieType.type}</option>
                    </c:forEach>
                </select>
                <br>
                <br>
                <div class="layui-upload">
                    <p>电影图片</p>
                    <input type="file"  class="layui-btn" id="movie_img_input"/><span></span>
                    <p>电影视频</p>
                    <input type="file" class="layui-btn"  id="movie_video_input"/><span></span>
<%--                    <button type="button" class="layui-btn" id="test1">上传图片</button>--%>
<%--                    <button type="button" class="layui-btn" id="test5"><i class="layui-icon"></i>上传视频</button>--%>
<%--                    <div class="layui-upload-list">--%>
<%--                        <img class="layui-upload-img" id="demo1">--%>
<%--                    </div>--%>
                </div>
            </div>
        </div>

        <div class="layui-form-item layui-form-text">
            <label for="movieDesc" class="layui-form-label">
                描述
            </label>
            <div class="layui-input-block">
                <textarea placeholder="请输入内容" id="movieDesc" name="synopsis" class="layui-textarea"></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">
            </label>
            <button  class="layui-btn"  id="movie_add_button">
                添加
            </button>
        </div>
    </form>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/lib/layui/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>
    layui.use('upload', function(){
        var $ = layui.jquery
            ,upload = layui.upload;

        //普通图片上传
        var uploadInst = upload.render({
            elem: '#test1'
            ,url: '/pictureUpload'
            ,accept: 'images' // 只允许上传图片
            ,acceptMime: 'image/*'  // 打开文件选择器时只显示图片
            ,auto:false
            ,size:''
            ,before: function(obj){
                //预读本地文件示例，不支持ie8
                obj.preview(function(index, file, result){
                    $('#demo1').attr('src', result); //图片链接（base64）
                    console.log("开始")
                });
            },
            after:function (obj) {
                console.log("上传完成")
            }
        });
        upload.render({
            elem: '#test5'
            ,url: '/videoUpload'
            ,accept: 'video' //视频
            ,done: function(res){
                console.log(res)
            },
            before:function (obj) {
                console.log("开始")
            },
            after:function (obj) {
                console.log("之后")
            }
        });
    });
    $('#movie_add_button').click(function () {
        let movieImg = document.getElementById("movie_img_input");
        let movieVideo = document.getElementById("movie_video_input");
        let formData = new FormData();
        formData.append('movieName',$("#username").val())
        formData.append('movieType',$("#movieType option:selected").val())
        formData.append('movieDesc',$("#movieDesc").val())
        formData.append('movieImg',movieImg.files[0]);
        formData.append('movieVideo',movieVideo.files[0]);
        $.ajax({
            async: true,
            url:"${pageContext.request.contextPath}/addMovie2",
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success:function (result) {
                console.log(result);
            }
            }
        )
        for (var [a, b] of formData.entries()) {
            console.log(a, b);
        }
    })
</script>
</body>

</html>