import com.cl.pojo.Movie;
import com.cl.service.Movie.MovieService;
import com.cl.service.Movie.MovieServiceImpl;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class Movie_test {
    MovieServiceImpl service=new MovieServiceImpl();
    @Test
    public void test(){
        System.out.println(123);
    }
    @Test
    public void test1(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        MovieService service = (MovieService) context.getBean("movieServiceImpl");
        List<Movie> movies = service.getMovieByNameOrType("", "");
        for (Movie movie : movies) {
            System.out.println(movie.toString());
        }
    }
    @Test
    public void test3(){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        MovieService service = (MovieService) context.getBean("movieServiceImpl");
        List<Movie> movies = service.getMovieList();
        for (Movie movie : movies) {
            System.out.println(movie.toString());
        }
    }
}
