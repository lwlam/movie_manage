package com.cl.service.user;

import com.cl.pojo.User;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 *
 * @author yy
 * @date 2020/12/15/0015 9:08
 *
 */

public interface UserService {


    User userLogin(String username, String password);

    int updatePassword(String password);
    int register(User user);
}
