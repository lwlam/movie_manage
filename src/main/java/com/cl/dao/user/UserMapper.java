package com.cl.dao.user;


import com.cl.pojo.User;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 *
 * @author yy
 * @date 2020/12/15/0015 8:48
 */

public interface UserMapper {

    User getLoginUser(String username);

    int updatePassword(String password);

    int register(User user);

}
