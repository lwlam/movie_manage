package com.cl.controller;


import com.cl.pojo.Movie;
import com.cl.service.Movie.MovieService;

import com.cl.utils.SaveFile;
import com.cl.utils.Upload;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.Date;

@Controller
public class FileController {
    @Resource
    private MovieService movieService;
    private Movie movie = new Movie();

    @RequestMapping("/pictureUpload")
    public String pictureUpload(@RequestParam("file") CommonsMultipartFile file , HttpServletRequest request, Model model) throws IOException {

        String uploadFileName = file.getOriginalFilename();
        int index = uploadFileName.indexOf(".");
        uploadFileName = uploadFileName.substring(index);
        Date date = new Date();
        uploadFileName = String.valueOf(date.getTime())+uploadFileName;
        if ("".equals(uploadFileName)){
            return "redirect:/index.jsp";
        }
        System.out.println("上传文件名 : "+uploadFileName);
        String path = request.getRealPath("/pictureUpload");
        System.out.println(path);
        File realPath = new File(path);
        if (!realPath.exists()){
            realPath.mkdir();
        }
        System.out.println("上传文件保存地址："+realPath);
        System.out.println(realPath.toString());
        movie.setImg("http://localhost:8080/pictureUpload/"+uploadFileName);
        InputStream is = file.getInputStream(); //文件输入流
        OutputStream os = new FileOutputStream(new
                File(realPath,uploadFileName)); //文件输出流
        int len=0;
        byte[] buffer = new byte[1024];
        while ((len=is.read(buffer))!=-1){
            os.write(buffer,0,len);
            os.flush();
        }
        os.close();
        is.close();
        return "redirect:/movieList.html";
    }

    @RequestMapping("/addMovie")
    public String AddMovie(String name,String type, String synopsis){
        movie.setName(name);
        movie.setSynopsis(synopsis);
        movie.setType(type);
        movieService.addMovie(movie);
        return "redirect:/movieList.html";
    }
    @ResponseBody
    @RequestMapping(value = "/addMovie2",method = RequestMethod.POST)
    public String AddMovie2(@RequestParam("movieName")String movieName,@RequestParam("movieType")String movieType,@RequestParam("movieDesc")String movieDesc,@RequestParam("movieImg")CommonsMultipartFile movieImg,@RequestParam("movieVideo")CommonsMultipartFile movieVideo,HttpServletRequest request) throws IOException {
//        String imgname = movieImg.getOriginalFilename();
//        String viedeoname = movieVideo.getOriginalFilename();
//        System.out.println(saveFileUtil.saveFile(movieImg, request, "img"));
//        System.out.println(saveFileUtil.saveFile(movieVideo, request, "video"));
//        System.out.println(imgname);
//        System.out.println(viedeoname);
        //        movie.setName(name);
//        movie.setSynopsis(synopsis);
//        movie.setType(type);
//        movieService.addMovie(movie);
//        return "redirect:/movieList.html";
        Movie movie=new Movie();
        movie.setName(movieName);
        movie.setSynopsis(movieDesc);
        movie.setType(movieType);
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        SaveFile bean = context.getBean("saveFile", SaveFile.class);
        String imgUrl = bean.savefile(movieImg, request);
        String videoUrl = bean.savefile(movieVideo, request);
        movie.setImg(imgUrl);
        movie.setVideo(videoUrl);
        movieService.addMovie(movie);
        return "success";
    }
    @RequestMapping("/videoUpload")
    public String videoUpload(@RequestParam("file") CommonsMultipartFile file , HttpServletRequest request, Model model) throws IOException {
        String uploadFileName = file.getOriginalFilename();
        int index = uploadFileName.indexOf(".");
        uploadFileName = uploadFileName.substring(index);
        Date date = new Date();
        uploadFileName = String.valueOf(date.getTime())+uploadFileName;
        if ("".equals(uploadFileName)){
            return "redirect:/index.jsp";
        }
        String path = request.getRealPath("/videoUpload");
        File realPath = new File(path);
        if (!realPath.exists()){
            realPath.mkdir();
        }
        System.out.println("上传文件保存地址："+realPath);
        movie.setVideo("http://localhost:8080/videoUpload/"+uploadFileName);
        InputStream is = file.getInputStream(); //文件输入流
        OutputStream os = new FileOutputStream(new
                File(realPath,uploadFileName)); //文件输出流
        int len=0;
        byte[] buffer = new byte[1024];
        while ((len=is.read(buffer))!=-1){
            os.write(buffer,0,len);
            os.flush();
        }
        os.close();
        is.close();
        return "redirect:/movieList.html";
    }
}

